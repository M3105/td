### TD

#### Choix Préparation 0:
---

- Je vous propose d'installer une distribution linux `devuan` ASCII pour les TD/TP.   

#### Choix Préparation 1:
---

- Télécharger sur `partage.rt-iut.re` une machine virtuelle linux. Je vous propose de prendre une devuan.  


#### Choix Préparation 2:
---
Pour les TD et TP il faudra utiliser des machines virtuelles. JE vous recommande d'utiliser `vagrant`

- Mettre a jour votre dépôt: `apt-get update` et si besoin `apt-get upgrade`
- Vérifier que VirtualBox est bien installé sur votre machine et est fonctionnel
- Installer `vagrant`: `apt-get install vagrant`
- Créez un répertoire dans lequel se trouvera votre machine virtuelle: `mkdir -p ~/v/devuan` ou `devuan` est le nom de la machine
- Positionnez-vous dans ce répertoire `cd v/devuan`
- Créer le fichier `Vagrantfile` dans ce répertoire (ou télécharger le [ici](https://gitlab.com/M3105/td/raw/master/files/Vagrantfile?inline=false) ou en faisant `wget https://gitlab.com/M3105/td/raw/master/files/Vagrantfile`)
```bash
Vagrant.configure(2) do |config|
  config.vm.box = "http://partage.rt-iut.re/rtcloud/VM/devuan_vg.box"
  config.ssh.username = "root"
  config.ssh.password = "toor"
  config.vm.guest = :debian
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.network :public_network
  config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh"
  config.ssh.paranoid = false
end
```
- Dans ce même répertoire, pour démarrer la machine virtuelle (c'est une machine `devuan` qui démarre):
```bash
vagrant up
```
- Connectez vous en ssh sur la machine virtuelle
```
vagrant ssh
```
- Pour quitter, faite un `CTRL+C`, et eteignez la machine avec la commande `vagrant halt`
- __ATTENTION__, il y a deux interfaces réseau, `eth0` et `eth1`. 
    - Vous ne devez pas modifier `eth0` c'est une interface d'administration
    - Vous devez configurer `eth1` pour vous connecter à Internet (route, passerelle, dhcp, dns etc...)
- Pour vous connecter a Internet via l'interface `eth1`, il vous faut
    - enlever la route par défaut passant par `eth0` avec la commande `route del ...` (à vous de compléter la commande)
    - puis passer par l'interface `eth1` avec la commande `route add default gw ...` (à vous de compléter la commande). Ou alors vous pouvez ici si disponible simplement relancer le dhcp sur `eth1`
    - Vous devez aussi changer l'addresse IP du DNS dans `/etc/resolv.conf`

- Vous pouvez  démarrer plusieurs instance de la machine. 
    - Pour cela il suffit de créer un nouveau répertoire et d'y copier le fichier `Vagrantfile`.
    - __ATTENTION__: dans ce fichier il faut modifier le port `host: 2222` pour éviter les conflits sur les ports

- NOTES (pas nécessaire pour vous normalement):
    - Pour exporter les affichages X exécuter `ssh -p 2222 -X vagrant@127.0.0.1` au lieu de `vagrant ssh`
    - `vagrant package` pour créer une box
    - Pour avoir une debian : `http://partage.rt-iut.re/rtcloud/VM/debian_jessie.box` dans votre `Vagrantfile`
    
    
#### TD
---

- [TD0](TD0.md)
- [TD1](TD1.md)