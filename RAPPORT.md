### Installation dun seveur DNS maitre et esclave.

- Vous devez rendre un rapport au format pdf
- Vos fichiers de configuration peuvent se trouver en ligne, et certaines annexes aussi (sur votre dépot git par exemple) et/ou dans le rapport. Vous pouvez même avoir une version en ligne de votre rapport sur votre dépôt git.
- Votre rapport doit être facile à lire et facile à parcourir et trouver des informations dans votre rapport doit être facile.
- Si vous n'avez pas réalisé l'intégralité du TP, cette information doit se trouver rapidement dans votre rapport
- Vous devez pouvoir montrer dans votre rapport que votre installation fonctionne
- Si votre installation ne fonctionne pas, vous devez pouvoir expliquer pourquoi
- Vous devez décrire étape par étape votre installation, soyez précis et exhaustif.
- Votre installation doit être facilement reproductible
- Vous devez décrire les outils et expliquez les commandes (pensez que les instructions de votre rapport doit pouvoir être reproduit et expliqué à un débutant)
- Vous devez décrire l'architecture que vous avez utilisé (IP, nom de machine etc...) avec des schémas si possible.
- Essayez de simplifier votre vie et de fournir un moyen de reproduire votre installation rapidement.
    


### Autre:

- Faites une vidéo tutoriel si vous avez le temps
- Il faut pouvoir retrouver facilement des informations dans votre rapport
- Essayez de ne pas supposer que la personne qui lit votre rapport a les mêmes connaissance que vous.
- Décrivez les problèmes que vous avez rencontrés et les moyens de les résoudres au cas ou celui qui doit reproduire votre TP se retrouve en face des mêmes problèmes. Une section "troubleshouting" est toujours appréciée.