### Commandes simples (DNS)


#### Effectuez une requête DNS manuellement: Domain Information Groper (dig)

Ici on enlève la recursion, et on effectue les requêtes DNS à la main.

Par exemple, si vous voulez connaitre l'adresse IP du site: `a.raza.re`

1. Le serveur DNS local n'a pas la connaissance sur l'IP affectée à `a.raza.re`
2. On doit trouver `raza.re`, `.re`
3. Pour savoir qui gère le `TLD`, `.re` il faut demander à un serveur racine. La liste des serveurs racine est connue de tous les serveurs DNS, c'est la seule information configurée en dur sur les serveurs DNS.

##### Etape 1: Pour avoir la liste des serveurs racines :
---

Installez la commande `dig` avec `apt-get install dnsutils`

```
$ dig . NS
```

> Q1: Que retourne cette commande ? 

##### Etape 2: TLD
---

Grace à la commande précédente, nous allons utiliser les informations pour trouver l'adresse IP du serveur `TDL` de `.re`

```
dig @h.root-servers.net re NS
```
Dans la commande précédent, vous n'utiliserez pas forcément le serveur `h.root-servers.net` et il faut mettre l'adresse IP de ce serveur et non pas son nom.

> Q2: Expliquez cette commande ? 

##### Etape 3: Authoritative
---

La commande précédente nous permet d'obtenir des informations permettant de continuer notre requête dns. Pour cela nous exécutons la commande:

```
dig @192.5.4.2 raza.re ns
```
Dans la commande précédente, vous l'utiliserez probablement pas la même adresse IP que celle donnée ci-dessus. 

> Q3: Expliquez cette commande ? 

##### Etape 4: FQDN
---

Maintenant, nous pouvons obtenir l'adresse ip avec la commande:

```
dig @ns107.ovh.net a.raza.re A
```

> Q3: Expliquez cette commande ? 

Pour effectuer cette dernière requête il faut passer par plusieurs requêtes qui permettent d'obtenir l'adresse IP de `ns107.ovh.net`

##### Notes
---
Une option intéressante de `dig` est `+trace` 