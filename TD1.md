## Sujet du TD1/TP1

#### Instruction pour le rapport
---

__Les instructions de rédaction du rapport sont [ici](https://gitlab.com/M3105/td/blob/master/RAPPORT.md)__



#### Un simple resolveur de DNS.
---

__VOUS DEVEZ FAIRE UN COMPTE RENDU POUR CE TD/TP.__ Aller sur http://moodle.univ-reunion.fr dans le cours M3105 pour plus d'infos et voir ce qui est attendu. Ne commencez pas votre TP avant d'avoir lu les instructions.

__Vous pouvez vous aidez de ce [lien](http://askubuntu.com/questions/330148/how-do-i-do-a-complete-bind9-dns-server-configuration-with-a-hostname).__ Prenez le temps de bien lire cette "documentation" en entier avant de commencer.


Pour ce TP, vous devez vous aidez de tutoriaux en ligne. En effet, une installation détaillé ici ne serait pas utile puisque les versions
changent, et les fichiers de configuration aussi. 

Démarrez la machine physique en Linux, puis, démarrer une VM linux. Je vous recommande de passer par `vagrant` (infos [ici](https://gitlab.com/M3105/td/blob/master/TD1.md)), mais vous faîtes ce que vous voulez.

Précédure simplifiée: 

1. Configurer la carte en bridge, retenez l'adresse IP de votre machine virtuelle et configurer la en dur
2. Faites un `apt-get update` (et si nécessaire `apt-get upgrade`).
3. Configurer votre ip de manière statique et de manière persistante
4. Editez le ficher `/etc/hostname` et mettre un nom d'hôte dans ce fichier avec les initiales données ci-dessus. C'est le nom de votre ordinateur et qui sera le serveur dns
5. Editez le fichier `/etc/hosts` et mettre renseignez les bonnes associations; Ici, vous gérez le domaine rt`{aa,bb,cc,...}`.local. Donc vous devriez avoir une ligne du type: 

```
172.40.10.59 tr.rt{aa,bb,cc,...}.local tr
```
    
    - `{aa,bb,cc,...}` correspond au nom de votre ordinateur liste d'initial ci-dessus.
    - `tr` correspond au nom de votre ordinateur liste d'initial ci-dessus.

Donc pour résumé et pour moi, je gère le domaine `rttr.local` et le nom de ma machine est `tr`. Donc j'aurais:

```
172.40.10.59 tr.rttr.local tr
```

6. Installez les paquets suivants :
```bash	
sudo apt-get install bind9 bind9utils dnsutils host
```
7. Configurez les fichiers suivants qui doivent se trouver dans `/etc/bind`
```
    named.conf.options
    named.conf.local
    /etc/resolv.conf
```

Pour information, le fichier principale de configuration de bind9 est `/etc/bind/named.conf`, ce fichier inclus les fichiers ci-dessus, qui eux même inclus vos fichiers de zones directes et de zones inverses.

    - Dans le fichier `named.conf.options` normalement, vous n'avez pas a éditer le fichier, pour le moment"
    - Dans le fichier `named.conf.local` vous devez défnir vos zones directes et inverses et les fichiers qui définissent ces zones.
    - Dans le fichier `/etc/resolv.conf` vous devez mettre le domaine que vous gérer, la recherche et votre address ip comme serveur dns

8. La configuration de base de bind vient déjà normalement avec des zones par défaut contenant les serveur `root`. Elles sont dans le fichier `/etc/bind/named.conf.default-zones`. Que vous ne touchez pas normalement mais desquels vous pouvez vous inspirer.
9. Vous devez donc créer vos fichiers de zones: directes ou inverses et placez les dans l'arbre global des zones DNS.
10. Configurer plusieurs machines pour qu'elle utilise votre serveur dns et tester le bon fonctionnement de votre serveur. (`dig tr.rttr.local`). Sur les autres machines n'oubliez pas de mettre l'IP de votre serveur dans `/etc/resolv.conf` et aussi le domaine `rttr.local`

##### Ensuite
__Attention__ Normalement, vous n'avez pas besoin d'éditer le fichier `named.conf.options` et votre serveur doit fonctionner.

11. On va transformer notre serveur bind en un simple resolver qui va aller interroger les serveurs `root` puis les serveurs `TLD` puis ceux ayant `autorité`.
    1. Editez `/etc/bind/named.conf.options` et ajoutez la ligne `recursion yes`;
    1. Faite une requête dns : `nslookup www.google.fr localhost`  (localhost à la fin signifie que l'on interroge le serveur de la machine locale). Qu'observez vous dans la réponse ?

12. Montrez l'effet du cache sur le temps de réponse des requêtes en demandant 2 fois une site particulier, par exemple `www.lemonde.fr`. Utilisez `dig` plutôt que `nslookup` et lire le temps de réponse ! Analysez le cache en le  « dumpant » dans le fichier spécifié dans `named.conf.options` avec la commande `rndc dumpdb -cache`.

13. Nous allons transformer notre serveur en proxy DNS en dé-commentant l'option `forwarder` et en mettant l'adresse IP d'un serveur dns par exemple `9.9.9.9`. Redémarrez ensuite le serveur et montrez que cela fonctionne.  Est-ce que le proxy fait du caching ? Montrez le en vidant le cache (`rndc flush`) puis en le lisant après une requête. 

##### Enfin
14. Installez un second serveur DNS en mode esclave (une doc [ici](http://www.microhowto.info/howto/configure_bind_as_a_slave_dns_server.html)).

#### Notes
---

Regardez  un exemple de fichier de zone directe et inverse sur Internet, par exemple sur 
- http://www.zytrax.com/books/dns/ch6/mydomain.html
- http://www.zytrax.com/books/dns/ch6/reverse-map.html

Tester vos fichiers de configuration et de zone au fur et à mesure avec les commandes : 
- `named-checkzone` 
- `named-checkconf`


